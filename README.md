# USTA.sh
Undo STAtus (USTA)
A tiny bash script that displays basic status information about the system 

# Requirements
- mawk 1.3.4 or greater (most likely preinstalled)
- GNU date 8.30 or greater (most likely preinstalled)
- GNU grep 3.4 or greater (most likely preinstalled)
- acpi 1.7 or greater (most likely Not preinstalled, necessary for displaying battery)

# Usage
It's a shell script but it has its environmental variable set to bash so it's highly recommended that 
you run it using bash.

you can run it using:
    `bash /path/to/status.sh`

to simplify this, you could use an alias such as:
    `alias status='bash path/to/status.sh'`

and to make the alias persistent every time you open the terminal 
simply add the above line to your `~/.bashrc`

to get fancier, you could use a while true loop 
to constantly update the output:
    `while true;
    do
    clear;
    bash /path/to/status.sh;
    sleep 5;
    done
    `
the short segment above will refresh the output every five seconds and 
it will also clear the screen every time it does so as to not clutter the
display with outdated information.

to add fluff to your already fancy setup, you could pipe the output to lolcat:
    `while true; 
    do
    clear;
    bash /path/to/status.sh | lolcat;
    sleep 5;
    done
    `

# Modifying the output
The script is very very easy to understand apart from the awk utility 
which can be very easily understood through the man page. 
The script has a few lines commented out by default (i.e the battery
indicator for users who don't own a laptop) or the home directory storage
display for users without a home directory. Please edit the script and modify it to 
your personal liking. 


## License
This is free software, you are free to change and redistribute it (GPL)



