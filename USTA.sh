#! /bin/bash

#Memory
echo "Memory:"
#Memory -- RAM
echo "    '-> RAM: $(free -h | grep Mem | awk '{print $3, "out of", $2}')"
#Memory -- Swap, Comment out if you don't have swap
#echo "    '-> SWAP: $(free -h | grep Swap | awk '{print $3, "out of", $2}')"


echo ""


#Battery life, Comment out if you don't use a laptop/tablet
#echo "Battery Life:"
#echo "    '-> $(acpi | awk '{print $4,$5,$6,$7,$8}')" 


#echo ""


#Storage
echo "Storage:"
#Storage -- Root partition
echo "    '-> $(df -h / | grep / | awk '{print "Root:", $3, "out of", $2}')"
#Storage -- Home partition, Comment out if you don't have a Home partition
#echo "    '-> $(df -h /home | grep /home | awk '{print "Home:",$3, "out of", $2}')"


echo ""


#Date
echo "Date and Time:"

#Date -- Style 1: Verbose
#echo "    '-> $(date +%Y" Years "%m" Months $(expr $(date +%W) % $(date +%m)) Weeks "%u" Days "%H" Hours "%M" Minutes "%S" Seconds")"

#Date -- Style 2: DD.MM.YYYY  HH:MM:SS
echo "    '-> $(date +%d"."%m"."%Y" "%X)"
